// console.log("Hello World");


//What are CONDITIONAL STATEMENTS?

	//Conditional Statements allow us to control the flow of our program
	//It allows us to run a statement/instruction if a condition is met or run another separate instructon if otherwise

	//if, else if, and else statement

		let numA = -1;

	//if statement
		//executes a statement if a specified condition is true

		if(numA<0){
			console.log("Yes! It is less than zero");
		}

	/*
		Syntax:

		if(condition){
			statement
		}
	*/
	//the result of the expression added in the if's condition must result to true, else, the statement inside if() will not run

		console.log(numA<0);	//results to true -- the if statement will run

		numA = 0;

		if(numA<0){
			console.log("Hello");
		}

		console.log(numA<0);	//results to false -- it will not print


		let city = "New York";

		if(city === "New York"){
			console.log("Welcome to New York City!");
		}



	//else if statement
		/*
			- excutes a statement if previous conditions are false and if the specified condition is true
			- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
		*/

		let numH = 1;

		if(numA<0){
			console.log("Hello");
		} else if (numH>0){
			console.log("World");
		}

		//we were able to run the else if() statement after we evaluated that the if condition was failed

		//if the if() condition was passed and run, we will no longer evaluate the else if() and end the process there

		numA = 1;

		if(numA>0){
			console.log("Hello");
		} else if (numH>0){
			console.log("World");
		}

		//else if() statement was no longer run because the if statement was able to run, the evaluatoin of the whole statement stops there

		city = "Tokyo";

		if(city === "New York"){
			console.log("Welcome to New York City!");
		} else if(city === "Tokyo"){
			console.log("Welcome to Tokyo, Japan!");
		}

		//since we failed the condition for the first if(), we went to the else if() and checked instead passed that condtion


	//else statement
	/*
		- executes a statement if all other conditions are false
		- the "else" statement is optional and can be added to capture any other result to change the flow of a program
	*/

		if(numA<0){
			console.log("Hello");
		}
		else if(numH === 0){
			console.log("World");
		}
		else {
			console.log("Again");
		}
		/*
			Since both the preceeding if and else if conditions are not met/failed, the else statement was run instead

			else statments should only be added if there is a preceeding if conditon

			else statments by itself will not work, however, if statements will work even if there is no else statement
		*/

		/*else{
			console.log("Will not run without an if");	//will cause an error
		}*/

		/*else if(numH === 0){
			console.log("World");
		}
		else{
			console.log("Again");
		}*/

		//same goes for an else if, there should be a preceeding if() first



	/* if, else if, and else statement with functions */
		/*
			- most of the time, we would like to use if, else if, and else statments with functions to control the flow of our application
		*/

		let message = "No message.";
		console.log(message);

		function determineTyphoonIntensity(windSpeed){

			if(windSpeed < 30){
				return 'Not a typhoon yet.';
			}
			else if(windSpeed <= 61){
				return 'Tropical depression detected,';
			}
			else if(windSpeed >= 62 && windSpeed <=88){
				return 'Tropical Storm detected.';
			}
			else if(windSpeed >= 89 || windSpeed <= 117){
				return 'Severe tropical storm detected.';
			}
			else {
				return 'Typhoon Detected';
			}

		};

		//returns the string to the variable "message" that invoked it
		message = determineTyphoonIntensity(100);
		console.log(message);

		if(message === "Severe tropical storm detected."){
			console.warn(message);
		}
		//console.ward() is a good way to print warnings in our console that could help us developers act on a certain output with our code


		//MINI ACTIVITY 1

		function oddOrEvenChecker(num){

			if(num % 2 === 0) {
				alert("This is even number");
			}
			else {
				alert("This is odd number");
			}
		}

		oddOrEvenChecker(5);


		//MINI ACTIVITY 2

		function ageChecker(age){

			if(age >= 18) {
				alert('You are Allowed to drink, party party!');
		//or 	alert(age + "are Allowed to drink, party party!");
				return true;
			}
			else {
				alert("You are not allowed to drink! gatas muna");
		//or 	alert(age + "You are not allowed to drink! gatas muna");
				return true;
				return false;
			}
		}

		isAllowedToDrink = ageChecker(25);
		console.log(isAllowedToDrink);



	//Truthy and Falsy 

		/*
			- In Javascript a "truthy" value is a value that is considered true when encountered in Boolean context
			- Values in are considered true unless defined otherwise
			- Falsy Values/exceptions for truth:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN
		*/


		//Truthy Examples
		/*
			- If the result of an expression in a condition results to a truthy value, the condtion returns true and the corresponding statements are executed
			- Expressions are any unit of code that can be evaluated to a value
		*/

		if(true){
			console.log("Truthy");
		}
		if(1){
			console.log("Truthy");
		}
		if([1,2,3]){
			console.log("Truthy");
		}


		//Falsy Examples

		if(false){
			console.log("Falsy");
		}

		if(0){
			console.log("Falsy");
		}
		if(undefined){
			console.log("Falsy");
		}
		
	

	//Conditional (Ternary) Operator
		/*
			- The Conditional (Ternary) Operator takes in three operands:
				1. condition
				2. expression to execute if the condtion is truthy
				3. expression to execute if the condtion is falsy

			- this ca be used as an alternative to an 'if else' statement
			- ternary operator have an implicit 'return' statement meaning that without the 'return' keyword, the resulting expressions can be stored in a variable

			*SYNTAX*
				(expression) ? ifTrue : ifFalse;
		*/

		//Single Statement Execution

			let ternaryResult = (1 < 18) ? true : false;
			console.log("Result of Ternary Operator: " + ternaryResult);
			console.log(ternaryResult);


		//Multiple Statement Execution
		/*
			- a function may be defined then used in a ternary operator
		*/

		let name;

		function isOfLegalAge() {
			name = "john";
			return "You are of the legal age limit";
		};

		function isUnderage() {
			name = "Jane";
			return "You are under the age limit";
		};

		let age = parseInt(prompt("What is your age?"));
		console.log(age);

		let legalAge = (age > 18) ? isOfLegalAg() : isUnderage();
		console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);


	//Switch Statement
	/*
		- The switch statement evaluates an expression and matches the expression's value to a case clause

		.toLowerCase function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case

		- The "expression" is the information used to match the "value" provided in the switch cases

		- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values

		- break statement is used to terminate the current loop once a match has been found
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day) {
		case 'monday':
			console.log("The color of the day is red " );
			break;
		case 'tuesday':
			console.log("The color of the day is yellow");
			break;
		case 'wednesday':
			console.log("The color of the day is orange");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is blue");
			break;
		case 'saturday':
			console.log("The color of the day is indigo");
			break;
		case 'sunday':
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please enter a valid date")
	}


		//MINI ACTIVITY 3

		function determineBear(bearNumber){
			let bear;

			switch (bearNumber){
				case 1:
					alert("Hi, I'm Amy!");
					break;
				case 2:
					alert("Hi, I'm Lulu!");
					break;
				case 3:
					alert("Hi, I'm Morgan!");
					break;
				default:
					bear = bearNumber + "is out of bounds.";
					break;
			};

			return bear;
		};

		determineBear(2);


	//Try-Catch-Finally Statement
	/*
		- 'try catch' statements are commonly used for error handling
		- they are used to specify a response whenever an error is received

	*/

	function showIntensityAlert(){
		try {
			alerat(determineTyphoonIntensity(windSpeed));
			//error/err are commontly used variable names used by developers for storing errors
		}
		catch (error){
			console.log(typeof error);
			//the error.message is used to access the information relating to an error object
			console.warn(error.message);
		}
		finally {
			//continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors
			alert("Intensity updates will show new alert.");
		}
	};

	showIntensityAlert(56);